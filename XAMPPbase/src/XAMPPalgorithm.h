#ifndef XAMPPBASE_XAMPPALGORITHM_H
#define XAMPPBASE_XAMPPALGORITHM_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>
#include <TStopwatch.h>
#include <XAMPPbase/ChronoHandle.h>

#include <string>

namespace XAMPP {
    class IAnalysisHelper;
    class ISystematics;
    class XAMPPalgorithm : public EL::AnaAlgorithm {
    public:
        XAMPPalgorithm(const std::string& name, ISvcLocator* pSvcLocator);
        virtual ~XAMPPalgorithm() = default;

        virtual StatusCode initialize();
        virtual StatusCode execute();
        virtual StatusCode finalize();
        virtual StatusCode beginInputFile() override;

    private:
        StatusCode CheckCutflow();
        StatusCode ExecuteEvent();

        ToolHandle<XAMPP::ISystematics> m_systematics;
        ToolHandle<XAMPP::IAnalysisHelper> m_helper;

        bool m_RunCutFlow;
        bool m_init;

        TStopwatch m_tsw;
        long long int m_Events;
        long long int m_CurrentEvent;
        long long int m_printInterval;
        unsigned int m_TotSyst;

        bool m_updateTotEvents;  // Update the number of total events at the beginning of each file
        bool m_doTime;           //> Use ChronoStatSvc to time the algorithm
        ChronoHandle m_initTimer{this, "initialize"};
        ChronoHandle m_executeTimer{this, "execute"};
        unsigned int m_TotFiles;
        unsigned int m_CurrentFile;
        std::string TimeHMS(float t) const;
    };

}  // namespace XAMPP
#endif  //> !XAMPPBASE_XAMPPALGORITHM_H
