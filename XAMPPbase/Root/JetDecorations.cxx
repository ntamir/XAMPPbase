#include <XAMPPbase/JetDecorations.h>

XAMPP::JetDecorations::JetDecorations() :
    ParticleDecorations(),
    isBadJet("bad"),
    isBJet("bjet"),
    isBJet_Loose("bjet_loose"),
    passJVT("passJvt"),
    passfJVT("passFJvt"),
    nTracks("NTrks"),
    jetAlgorithm("JetAlgorithm"),
    BTagWeight("BTagScore"),
    DFCommonJets_jetClean_LooseBad("DFCommonJets_jetClean_LooseBad"),
    rawJVT("Jvt"),
    DetectorEta("DetectorEta") {}

void XAMPP::JetDecorations::populateDefaults(SG::AuxElement& ipart) {
    isBadJet.set(ipart, false);
    isBJet.set(ipart, false);
    isBJet_Loose.set(ipart, false);
    nTracks.set(ipart, -1);
    jetAlgorithm.set(ipart, -1);
    BTagWeight.set(ipart, -1e9);
    rawJVT.set(ipart, 0.);
    // do not touch flags from the xAOD
}
