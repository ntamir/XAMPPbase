#!/env/python
from ClusterSubmission.Utils import ResolvePath, ReadListFromFile, WriteList
from XAMPPmultilep.SubmitToGrid import GetDSID
from XAMPPbase.UpdateSampleLists import setupArgParser
from XAMPPbase.CreateAODFromDAODList import convertToAOD
from ClusterSubmission.AMIDataBase import getAMIDataBase

import os, logging
if __name__ == "__main__":
    options = setupArgParser().parse_args()
    Smp_Dir = ResolvePath(options.ListDir)
    missing_prw_aod = []
    DSIDs = []
    ### Get the lits of unordered samples
    for l in os.listdir(Smp_Dir):
        datasets = ReadListFromFile("%s/%s" % (Smp_Dir, l))
        for smp in sorted(datasets):
            mc_channel = GetDSID(smp)
            if mc_channel < 0: continue
            ami_tag = smp[smp.rfind(".") + 1:]

            camp = ""
            if len([x for x in options.mc16aTag if ami_tag.find(x) != -1]) > 0: camp = "a"
            elif len([x for x in options.mc16dTag if ami_tag.find(x) != -1]) > 0: camp = "d"
            elif len([x for x in options.mc16eTag if ami_tag.find(x) != -1]) > 0: camp = "e"
            else:
                logging.warning("Unkown mc campaign in sample %s" % (smp))
                continue
            cfg_file = ResolvePath("dev/PileupReweighting/share/DSID%sxxx/pileup_mc16%s_dsid%s_%s.root" %
                                   (str(mc_channel)[0:3], camp, mc_channel, "AFII" if ami_tag.find("_a") != -1 else "FS"))
            if not cfg_file:
                DSIDs += [mc_channel]
                missing_prw_aod += [convertToAOD(smp)]

    getAMIDataBase().getMCDataSets(channels=DSIDs, derivations=["NTUP_PILEUP"])

    missing_prw = []
    for dsid in DSIDs:
        pyami_ch = getAMIDataBase().getMCchannel(dsid=dsid)
        prw_tags = [
            t for t in pyami_ch.getTags(data_type="NTUP_PILEUP")
            if len([x for x in options.mc16aTag + options.mc16dTag + options.mc16eTag if t.find(x) > 0]) > 0 and len(t.split("_")) == 4
        ]
        missing_prw += ["%s.%d.%s.deriv.NTUP_PILEUP.%s" % (pyami_ch.campaign(), pyami_ch.dsid(), pyami_ch.name(), t) for t in prw_tags]
    if len(missing_prw) > 0: WriteList(missing_prw, "%s/PRW_not_copied.txt" % (os.getcwd()))
    for x in missing_prw:
        logging.info("Missing prw file" + x)

    prw_avail = [convertToAOD(cfg) for cfg in missing_prw]
    missing_prw_aod = [aod for aod in missing_prw_aod if aod not in prw_avail]
    if len(missing_prw_aod) > 0: WriteList(missing_prw_aod, "%s/PRW_not_made.txt" % (os.getcwd()))
